#include <iostream>
#include <cassert>

int factorial(int n)
{
  auto ret = 1;
  for(auto j = n; j - 1; --j)
  {
    ret *= j;
  }
  return ret;
}

template< int N >
class Factorial
{
  public:
    enum
    {
      value = N * Factorial< N - 1 >::value
    };
};
template< >
class Factorial< 0 >
{
  public:
    enum
    {
      value = 1
    };
};

int even_factorial(int n)
{
  auto ret = 1;
  for(auto j = n; j - 1; --j)
  {
    if(j % 2)
    {
      ret *= j;
    }
  }
  return ret;
}

template< int N >
class Even_factorial
{
  public:
    enum
    {
      value = (N % 2 ? N : 1) * Even_factorial< N - 1 >::value
    };
};

template< >
class Even_factorial< 0 >
{
  public:
    enum
    {
      value = 1
    };
};

int odd_factorial(int n)
{
  auto ret = 1;
  for(auto j = n; j - 1; --j)
  {
    if(!(j % 2))
    {
      ret *= j;
    }
  }
  return ret;
}

template< int N, bool isEven >
class Odd_factorial_impl
{};
template< int N >
using Odd_factorial = Odd_factorial_impl< N, N % 2 >;

template< int N >
class Odd_factorial_impl< N, true >
{
  public:
    enum
    {
      value = 1 * Odd_factorial_impl< N - 1, (N - 1) % 2 >::value
    };
};

template< int N >
class Odd_factorial_impl< N, false >
{
  public:
    enum
    {
      value = N * Odd_factorial_impl< N - 1, (N - 1) % 2 >::value
    };
};

template< >
class Odd_factorial_impl< 1, true >
{
  public:
    enum
    {
      value = 1
    };
};

constexpr int ct_factorial(int n)
{
  return n ? n * ct_factorial(n - 1) : 1;
}

int main()
{
  constexpr auto ARG = 4;
  constexpr auto CT_FACTORIAL = ct_factorial(ARG);

  auto f = factorial(ARG);
  auto odd_f = odd_factorial(ARG);
  auto even_f = even_factorial(ARG);

  constexpr auto cexp_f = Factorial< ARG >::value;
  constexpr auto cexp_odd_f = Odd_factorial< ARG >::value;
  constexpr auto cexp_even_f = Even_factorial< ARG >::value;

  static_assert(cexp_f == cexp_odd_f * cexp_even_f, "In compile time via metaprograms");
  static_assert(CT_FACTORIAL == cexp_f, "In compile time via constexpr and metaprogram");

  assert(f == cexp_f);
  assert(odd_f == cexp_odd_f);
  assert(even_f == cexp_even_f);

  std::cout << "\nFactorial " << ARG << ": " << cexp_f;
  std::cout << "\n     even " << ARG << ": " << cexp_even_f;
  std::cout << "\n      odd " << ARG << ": " << cexp_odd_f;
  std::cout << std::endl;
}
